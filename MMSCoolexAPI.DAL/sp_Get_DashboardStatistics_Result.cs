//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MMSCoolexAPI.DAL
{
    using System;
    
    public partial class sp_Get_DashboardStatistics_Result
    {
        public string Dashboard { get; set; }
        public Nullable<int> Users { get; set; }
        public Nullable<int> Contracts { get; set; }
        public Nullable<int> WorkOrderCompleted { get; set; }
        public Nullable<int> WorkOrderPending { get; set; }
    }
}
