//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MMSCoolexAPI.DAL
{
    using System;
    
    public partial class sp_Get_Contract_types_Result
    {
        public int TB_ID { get; set; }
        public string SER_TYP { get; set; }
        public string SER_TYPE_DESC { get; set; }
        public string ENTRY_USER_NAME { get; set; }
        public Nullable<System.DateTime> ENTRY_DATE { get; set; }
        public string UPDATE_USER_NAME { get; set; }
        public Nullable<System.DateTime> UPDATE_DATE { get; set; }
        public string SER_TYPE_DESC_A { get; set; }
    }
}
