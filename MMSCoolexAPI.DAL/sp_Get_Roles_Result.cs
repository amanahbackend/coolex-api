//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MMSCoolexAPI.DAL
{
    using System;
    
    public partial class sp_Get_Roles_Result
    {
        public int TB_ID { get; set; }
        public string TB_NAME { get; set; }
        public string TB_PERMISSIONS { get; set; }
        public Nullable<int> TB_DELETE { get; set; }
    }
}
