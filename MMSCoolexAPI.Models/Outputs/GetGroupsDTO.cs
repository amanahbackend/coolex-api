﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class GetGroupsDTO
    {
        public List<Group> groups { get; set; }
    }
}
