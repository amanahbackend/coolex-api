﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
 public   class ActionFault
    {
        public string actionid { get; set; }
        public string faultcode { get; set; }
        public string actioncode { get; set; }
        public string actiondesc { get; set; }
    }
}
