﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class WorkOrderByGroupDTO
    {
        public List<WorkOrder> workorders { get; set; }
    }
}
