﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class customer
    {
        public string customerid { get; set; }
        public string custcd { get; set; }
        public string customername { get; set; }
        public string mobile { get; set; }
        public string telephone { get; set; }
        public List<customeraddress> customeraddresses { get; set; }
    }
}
