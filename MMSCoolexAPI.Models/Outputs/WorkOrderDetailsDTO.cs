﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class WorkOrderDetailsDTO
    {
        public string woid { get; set; }
        public string wono { get; set; }
        public string manualwono { get; set; }
        public string complno { get; set; }
        //public string custcd { get; set; }
        //public customer customer { get; set; }
        public string groupcd { get; set; }
        public string type { get; set; }
        public string servicetype { get; set; }
        public string contractno { get; set; }
        public string techtimein { get; set; }
        public string techtimeout { get; set; }
        public string custcd { get; set; }
        public string customername { get; set; }
        public string area { get; set; }
        public string street { get; set; }
        public string block { get; set; }
        public string plot_no { get; set; }
        public string avenue { get; set; }
        public string houseno { get; set; }
        public string addresscivilid { get; set; }
        public string civilid { get; set; }
        public string mobile { get; set; }
        public string telephone { get; set; }
        //public WOStatus wostatus { get; set; }
        public wostatusaction wostatusaction { get; set; }
        public string remarks { get; set; }
        public List<AddEquipmentDTO> equipment { get; set; }
        public List<AddUsedItemsDTO> items { get; set; }

    }
}
