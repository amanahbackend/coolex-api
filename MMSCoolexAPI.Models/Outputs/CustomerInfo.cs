﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
	public class CustomerInfo
	{
		public string custcd { get; set; }
		public string customername { get; set; }
		public string mobile { get; set; }
		public string telephone { get; set; }
	}
}
