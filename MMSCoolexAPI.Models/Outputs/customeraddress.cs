﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class customeraddress
    {
        public string customeraddressid { get; set; }
        public string custcd { get; set; }
        public string addressname { get; set; }
        public string area { get; set; }
        public string street { get; set; }
        public string block { get; set; }
        public string plot_no { get; set; }
        public string avenue { get; set; }
        public string houseno { get; set; }
        public string geolati { get; set; }
        public string geolong { get; set; }

    }
}
