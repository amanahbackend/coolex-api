﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class LOV
    {
        public string lovid { get; set; }
        public string name { get; set; }
        public string namear { get; set; }
        public string type { get; set; }
    }
}
