﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
   public class Brands
    {
        public string brandcd { get; set; }
        public string brandname { get; set; }
    }
}
