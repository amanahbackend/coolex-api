﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class WorkOrder
    {
        public string woid { get; set; }
        public string wono { get; set; }
        public string manualwono { get; set; }
        public string complno { get; set; }
        public string custcd { get; set; }
        public string groupcd { get; set; }
        public string type { get; set; }
        public string servicetype { get; set; }
        public string contractno { get; set; }
        public string customerremark { get; set; }
        public string techremark { get; set; }
        //public Priority wopriority { get; set; }
        //public WOStatus wostatus { get; set; }
		public WorkOrderAddress orderAddress { get; set; }
		public CustomerInfo customerInfo { get; set; }
		public wostatusaction wostatusaction { get; set; }
		public ContractInfo contractInfo { get; set; }
	}
}
