﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class Items
    {
        public string itemcode { get; set; }
        public string itemname { get; set; }
        public string uom { get; set; }
    }
}
