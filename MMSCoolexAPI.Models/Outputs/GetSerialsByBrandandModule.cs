﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
  public  class GetSerialsByBrandandModule
    {
        public string brand { get; set; }
        public string module { get; set; }
        public string itemcode { get; set; }
        public string serialnumber { get; set; }
    }
}
