﻿namespace MMSCoolexAPI.Models.Outputs
{
	public class ContractInfo
	{
		public string ExpirationDate { get; set; }
		public string ProjectNo { get; set; }
	}
}