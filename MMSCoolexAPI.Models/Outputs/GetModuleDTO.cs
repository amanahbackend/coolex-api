﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
  public  class GetModuleDTO
    {
        public List<Module> modules { get; set; }
    }
}
