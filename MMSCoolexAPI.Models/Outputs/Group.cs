﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class Group
    {
        public string groupid { get; set; }
        public string groupcd { get; set; }
        public string groupdesc { get; set; }
        public string vanno { get; set; }
        public string vandesc { get; set; }
        public string employeeno { get; set; }
        public string employeename { get; set; }
        public string remarks { get; set; }
    }
}
