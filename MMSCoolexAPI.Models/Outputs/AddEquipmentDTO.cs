﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class AddEquipmentDTO
    {
        public string wono { get; set; }
        public string brandcd { get; set; }
        public string modulecd { get; set; }
        public string serialnumber { get; set; }
        public string volt { get; set; }
        public string amp { get; set; }
        public string spr { get; set; }
        public string dpr { get; set; }
        public string roomtemperature { get; set; }
        public string ambtemperature { get; set; }
        public string sducttemp { get; set; }
        public string rducttemp { get; set; }
        public string group { get; set; }
        public ActionFault action { get; set; }
        public Fault fault { get; set; }
    }
}
