﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class WOStatus
    {
        public string wostatuslovid { get; set; }
        public string wostatusname { get; set; }

    }
}
