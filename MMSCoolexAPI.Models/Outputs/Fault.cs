﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class Fault
    {
        public string faultid { get; set; }
        public string faultcode { get; set; }
        public string faultdesc { get; set; }
    }
}
