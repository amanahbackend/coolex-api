﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
	public class WorkOrderAddress
	{
		public string area { get; set; }
		public string street { get; set; }
		public string block { get; set; }
		public string plot_no { get; set; }
		public string avenue { get; set; }
		public string houseno { get; set; }
	}
}
