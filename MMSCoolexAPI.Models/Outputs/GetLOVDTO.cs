﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class GetLOVDTO
    {
        public List<LOV> lovs { get; set; }
    }
}
