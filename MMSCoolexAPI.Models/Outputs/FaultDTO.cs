﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class FaultDTO
    {
        public List<Fault> faults { get; set; }
    }
}
