﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
   public class Module
    {
        public string brandcd { get; set; }
        public string modulecd { get; set; }
        public string modulename { get; set; }
        public string capacity { get; set; }

    }
}
