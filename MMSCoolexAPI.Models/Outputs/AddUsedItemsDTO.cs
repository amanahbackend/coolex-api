﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Outputs
{
    public class AddUsedItemsDTO
    {
        public string wono { get; set; }
        public string unitserialno { get; set; }
        public string Itemcode { get; set; }
        public string uom { get; set; }
        public string module { get; set; }
        //public string price { get; set; }
        public string quantity { get; set; }
    }
}
