﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models
{
    public class Status
    {
        public string statuscode { get; set; }
        public string statusdescription { get; set; }
        public string info1 { get; set; }
        public string info2 { get; set; }
        //public Translations translations { get; set; }

        public Status()
        {

        }

        public Status(int i)
        {
            switch (i)
            {
                case 0:
                    {
                        statuscode = "0";
                        statusdescription = "SUCCESS";
                        break;
                    }
                case 1:
                    {
                        statuscode = "1";
                        statusdescription = "FAILED";
                        break;
                    }

                case 404:
                    {
                        statuscode = "404";
                        statusdescription = "UNAUTHORIZED";
                        break;
                    }
                case 500:
                    {
                        statuscode = "500";
                        statusdescription = "INVALID MOBILE NUMBER";
                        break;
                    }
                case 600:
                    {
                        statuscode = "600";
                        statusdescription = "PINS UNAVAILABLE";
                        break;
                    }
                case 700:
                    {
                        statuscode = "700";
                        statusdescription = "REFERENCE NUMBER NOT FOUND";
                        break;
                    }
                case 701:
                    {
                        statuscode = "701";
                        statusdescription = "PAYMENT ALREADY DONE";
                        break;
                    }
                case 800:
                    {
                        statuscode = "800";
                        statusdescription = "REQUIRED PARAMETERS MISSING/EMPTY";
                        break;
                    }
                case 1000:
                    {
                        statuscode = "1000";
                        statusdescription = "SERVICE NOT FOUND";
                        break;
                    }
                case 1010:
                    {
                        statuscode = "1010";
                        statusdescription = "INVALID HASH";
                        break;
                    }
                case 1020:
                    {
                        statuscode = "1020";
                        statusdescription = "TAMPERED DATA";
                        break;
                    }
                case 1040:
                    {
                        statuscode = "1040";
                        statusdescription = "INVALID USER/LOGIN";
                        break;
                    }

                case 1060:
                    {
                        statuscode = "1060";
                        statusdescription = "SERVICE DISABLED/INVALID SERVICE";
                        break;
                    }
                case 1080:
                    {
                        statuscode = "1060";
                        statusdescription = "AMOUNT EXCEEDS";
                        break;
                    }
                case 2000:
                    {
                        statuscode = "2000";
                        statusdescription = "MOBILE NUMBER ALREADY EXISTS";
                        break;
                    }
                case 2020:
                    {
                        statuscode = "2020";
                        statusdescription = "EMAIL ALREADY EXISTS";
                        break;
                    }
                case 2040:
                    {
                        statuscode = "2040";
                        statusdescription = "INVALID VERIFICATION CODE";
                        break;
                    }
                case 2060:
                    {
                        statuscode = "2060";
                        statusdescription = "USERNAME DOESN'T EXISTS";
                        break;
                    }
                case 2080:
                    {
                        statuscode = "2080";
                        statusdescription = "USER IS DISABLED. PLEASE CONTACT SUPPORT";
                        break;
                    }
                case 3000:
                    {
                        statuscode = "2000";
                        statusdescription = "MOBILE NUMBER & EMAIL ALREADY EXISTS";
                        break;
                    }
                case 3020:
                    {
                        statuscode = "3020";
                        statusdescription = "MOBILE NOT ACTIVATED";
                        break;
                    }
                default:
                    {
                        statuscode = "";
                        statusdescription = "";
                        break;
                    }
            }




        }



    }
}
