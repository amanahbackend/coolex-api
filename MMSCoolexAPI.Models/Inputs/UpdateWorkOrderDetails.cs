﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Inputs
{
    public class UpdateWorkOrderDetails
    {
        public string woid { get; set; }
        public string wostatusid { get; set; }
        public string wostatusactionid { get; set; }
        public string techremark { get; set; }
        public string customerremark { get; set; }
    }
}
