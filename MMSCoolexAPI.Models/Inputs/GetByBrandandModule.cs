﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Inputs
{
    public class GetByBrandandModule
    {
        public string brand { get; set; }
        public string module { get; set; }
    }
}
