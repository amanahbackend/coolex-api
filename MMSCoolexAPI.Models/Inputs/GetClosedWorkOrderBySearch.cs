﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Inputs
{
  public  class GetClosedWorkOrderBySearch
    {
        public string wono { get; set; }
        public string complno { get; set; }
        public string custcd { get; set; }
        public string groupcd { get; set; }
    }
}
