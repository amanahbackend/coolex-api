﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMSCoolexAPI.Models.Inputs
{
  public  class UpdateWorkOrderStatus
    {
        public string woid { get; set; }
        public string wostatusid { get; set; }
        public string groupcd { get; set; }
        //public string wostatusactionid { get; set; }
    }
}
