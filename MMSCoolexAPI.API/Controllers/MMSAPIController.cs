﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MMSCoolexAPI.BL.Implementations;
using MMSCoolexAPI.BL.Interface;
using MMSCoolexAPI.Models.Inputs;
using MMSCoolexAPI.Models.Outputs;
using MMSCoolexAPI.Models;
using MMSCoolexAPI.API;
using System.Web.Http.Controllers;

namespace MMSCoolexAPI.API.Controllers
{
    //[auth]
    public class MMSAPIController : ApiController
    {
        IMMS mms = new MMS();

        [HttpPost]
        public DTO<LoginDTO> Login(Input<Login> obj)
        {
            return mms.Login(obj);
        }

        [HttpPost]
        public DTO<WorkOrderByGroupDTO> GetWorkOrdersByGroup(Input<WorkOrderByGroup> obj)
        {
            return mms.GetWorkOrdersByGroup(obj);
        }

        [HttpPost]
        public DTO<WorkOrderDetailsDTO> GetWorkOrderDetails(Input<WorkOrderDetails> obj)
        {
            return mms.GetWorkOrderDetails(obj);
        }
        [HttpPost]
        public DTO<GetLOVDTO> GetALLLOVs(Input<GetLOV> obj)
        {
            return mms.GetALLLOVs(obj);
        }
        [HttpPost]
        public DTO<GetLOVDTO> GetLOVByType(Input<GetLOVbyType> obj)
        {
            return mms.GetLOVByType(obj);
        }
        [HttpPost]
        public DTO<wostatusactionDTO> GetWorkOrderStatusAction(Input<MMSCoolexAPI.Models.Inputs.wostatusaction> obj)
        {
            return mms.GetWorkOrderStatusAction(obj);
        }
        [HttpPost]
        public DTO<FaultDTO> GetFaults(Input<GetFault> obj)
        {
            return mms.GetFaults(obj);
        }
        [HttpPost]
        public DTO<GetAactionFaultDTO> GetActionFaults(Input<GetActionFault> obj)
        {
            return mms.GetActionFaults(obj);
        }
        [HttpPost]
        public DTO<GetGroupsDTO> GetGroups(Input<GetGroups> obj)
        {
            return mms.GetGroups(obj);
        }
        //[HttpPost]
        //public DTO<UpdateWorkOrderStatusDTO> UpdateWorkOrderStatus(Input<UpdateWorkOrderStatus> obj)
        //{
        //    return mms.UpdateWorkOrderStatus(obj);
        //}
        [HttpPost]
        public DTO<GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersByGroup(Input<GetClosedWorkOrdersByGroup> obj)
        {
            return mms.GetClosedWorkOrdersByGroup(obj);
        }
        [HttpPost]
        public DTO<GetBrandsDTO> GetBrands(Input<GetBrands> obj)
        {
            return mms.GetBrands(obj);
        }
        [HttpPost]
        public DTO<GetModuleDTO> GetModules(Input<GetModule> obj)
        {
            return mms.GetModules(obj);
        }
        [HttpPost]
        public DTO<GetItemDescriptionDTO> GetItemDescription(Input<GetItemDescription> obj)
        {
            return mms.GetItemDescription(obj);
        }
        [HttpPost]
        public DTO<GetBySerialNumberDTO> GetBySerialNumber(Input<GetBySerialNumber> obj)
        {
            return mms.GetBySerialNumber(obj);
        }
        [HttpPost]
        public DTO<GetByBrandandModuleDTO> GetByBrandandModule(Input<MMSCoolexAPI.Models.Inputs.GetByBrandandModule> obj)
        {
            return mms.GetByBrandandModule(obj);
        }
        [HttpPost]
        public DTO<AddEquipmentDTO> AddEquipment(Input<AddEquipment> obj)
        {
            return mms.AddEquipment(obj);
        }
        [HttpPost]
        public DTO<AddUsedItemsDTO> AddUsedItems(Input<AddUsedItems> obj)
        {
            return mms.AddUsedItems(obj);
        }
        [HttpPost]
        public DTO<UpdateWorkOrderDetailsDTO> UpdateWorkOrderDetails(Input<UpdateWorkOrderDetails> obj)
        {
            return mms.UpdateWorkOrderDetails(obj);
        }
        [HttpPost]
        public DTO<GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersbySearch(Input<GetClosedWorkOrderBySearch> obj)
        {
            return mms.GetClosedWorkOrdersbySearch(obj);
        }

    }


    public class auth : BasicAuthenticationFilter
    {

        public auth()
        { }

        public auth(bool active)
            : base(active)
        { }


        public bool Authenticate(string username, string password)
        {
            if (username == "coolexappuser" && password == "coolex@pp969")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            if (username == "coolexappuser" && password == "coolex@pp969")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}