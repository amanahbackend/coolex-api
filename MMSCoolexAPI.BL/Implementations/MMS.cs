﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMSCoolexAPI.BL.Interface;
using MMSCoolexAPI.Models;
using MMSCoolexAPI.DAL;
using MMSCoolexAPI.Models.Outputs;

namespace MMSCoolexAPI.BL.Implementations
{
	public class MMS : IMMS
	{
		mms_dbEntities db = new mms_dbEntities();
		DAL.TB_GROUPS group = new TB_GROUPS();

		#region Public Methods


		public DTO<Models.Outputs.LoginDTO> Login(Input<Models.Inputs.Login> obj)
		{
			DTO<Models.Outputs.LoginDTO> dto = new DTO<Models.Outputs.LoginDTO>();
			Models.Outputs.LoginDTO resp = new Models.Outputs.LoginDTO();
			dto.objname = "Login";
			try
			{
				if (string.IsNullOrEmpty(obj.input.username) || string.IsNullOrEmpty(obj.input.password))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				if (!isvalidlogin(obj.input.username, obj.input.password))
				{
					dto.status = new Models.Status(1040);
					return dto;
				}

				Group grp = new Group();
				grp.groupid = group.TB_ID.ToString();
				grp.groupcd = group.GROUP_CD;
				grp.groupdesc = group.GROUP_DSC;
				grp.vanno = group.VAN_NO;
				grp.vandesc = group.VAN_DSC;
				grp.employeeno = group.EMPLOYEE_NUMBER;
				grp.employeename = group.EMPLOYEE_NAME;
				grp.remarks = group.REMARKS;

				resp.group = grp;
				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.WorkOrderByGroupDTO> GetWorkOrdersByGroup(Input<Models.Inputs.WorkOrderByGroup> obj)
		{
			DTO<Models.Outputs.WorkOrderByGroupDTO> dto = new DTO<Models.Outputs.WorkOrderByGroupDTO>();
			Models.Outputs.WorkOrderByGroupDTO resp = new Models.Outputs.WorkOrderByGroupDTO();
			dto.objname = "GetWorkOrdersByGroup";
			try
			{
				if (string.IsNullOrEmpty(obj.input.groupcd))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getworkordersbygroup(obj.input.groupcd);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.WorkOrderDetailsDTO> GetWorkOrderDetails(Input<Models.Inputs.WorkOrderDetails> obj)
		{
			DTO<Models.Outputs.WorkOrderDetailsDTO> dto = new DTO<Models.Outputs.WorkOrderDetailsDTO>();
			Models.Outputs.WorkOrderDetailsDTO resp = new Models.Outputs.WorkOrderDetailsDTO();
			dto.objname = "GetWorkOrderDetails";
			try
			{
				if (string.IsNullOrEmpty(obj.input.woid))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getworkordersbyid(Convert.ToInt32(obj.input.woid));

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetLOVDTO> GetALLLOVs(Input<Models.Inputs.GetLOV> obj)
		{
			DTO<Models.Outputs.GetLOVDTO> dto = new DTO<Models.Outputs.GetLOVDTO>();
			Models.Outputs.GetLOVDTO resp = new Models.Outputs.GetLOVDTO();
			dto.objname = "GetALLLOVs";
			try
			{
				resp = getalllovs();

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetLOVDTO> GetLOVByType(Input<Models.Inputs.GetLOVbyType> obj)
		{
			DTO<Models.Outputs.GetLOVDTO> dto = new DTO<Models.Outputs.GetLOVDTO>();
			Models.Outputs.GetLOVDTO resp = new Models.Outputs.GetLOVDTO();
			dto.objname = "GetLOVByType";
			try
			{
				if (string.IsNullOrEmpty(obj.input.type))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getlovsbytype(obj.input.type);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.wostatusactionDTO> GetWorkOrderStatusAction(Input<Models.Inputs.wostatusaction> obj)
		{
			DTO<Models.Outputs.wostatusactionDTO> dto = new DTO<Models.Outputs.wostatusactionDTO>();
			Models.Outputs.wostatusactionDTO resp = new Models.Outputs.wostatusactionDTO();
			dto.objname = "GetWorkOrderStatusAction";
			try
			{
				if (string.IsNullOrEmpty(obj.input.wostatuslovid))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getstatusaction(obj.input.wostatuslovid);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.FaultDTO> GetFaults(Input<Models.Inputs.GetFault> obj)
		{
			DTO<Models.Outputs.FaultDTO> dto = new DTO<Models.Outputs.FaultDTO>();
			Models.Outputs.FaultDTO resp = new Models.Outputs.FaultDTO();
			dto.objname = "GetFaults";
			try
			{

				resp = getfaults();

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;

		}
		public DTO<Models.Outputs.GetAactionFaultDTO> GetActionFaults(Input<Models.Inputs.GetActionFault> obj)
		{
			DTO<Models.Outputs.GetAactionFaultDTO> dto = new DTO<Models.Outputs.GetAactionFaultDTO>();
			Models.Outputs.GetAactionFaultDTO resp = new Models.Outputs.GetAactionFaultDTO();
			dto.objname = "GetActionFaults";
			try
			{

				if (string.IsNullOrEmpty(obj.input.faultcode))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getactionfault(obj.input.faultcode);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetGroupsDTO> GetGroups(Input<Models.Inputs.GetGroups> obj)
		{
			DTO<Models.Outputs.GetGroupsDTO> dto = new DTO<Models.Outputs.GetGroupsDTO>();
			Models.Outputs.GetGroupsDTO resp = new Models.Outputs.GetGroupsDTO();
			dto.objname = "GetGroups";
			try
			{

				resp = getgroups();

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.UpdateWorkOrderStatusDTO> UpdateWorkOrderStatus(Input<Models.Inputs.UpdateWorkOrderStatus> obj)
		{
			DTO<Models.Outputs.UpdateWorkOrderStatusDTO> dto = new DTO<Models.Outputs.UpdateWorkOrderStatusDTO>();
			Models.Outputs.UpdateWorkOrderStatusDTO resp = new Models.Outputs.UpdateWorkOrderStatusDTO();
			dto.objname = "UpdateWorkOrderStatus";
			try
			{
				if (string.IsNullOrEmpty(obj.input.woid) || string.IsNullOrEmpty(obj.input.wostatusid) || string.IsNullOrEmpty(obj.input.groupcd))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = updateworkorder(Convert.ToInt32(obj.input.woid), Convert.ToInt32(obj.input.wostatusid), obj.input.groupcd);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersByGroup(Input<Models.Inputs.GetClosedWorkOrdersByGroup> obj)
		{
			DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> dto = new DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO>();
			Models.Outputs.GetClosedWorkOrdersbyGroupDTO resp = new Models.Outputs.GetClosedWorkOrdersbyGroupDTO();
			dto.objname = "GetClosedWorkOrdersByGroup";
			try
			{
				if (string.IsNullOrEmpty(obj.input.groupcd))
				{
					dto.status = new Models.Status(800);
					return dto;
				}

				resp = getclosedworkordergroup(obj.input.groupcd);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetBrandsDTO> GetBrands(Input<Models.Inputs.GetBrands> obj)
		{
			DTO<Models.Outputs.GetBrandsDTO> dto = new DTO<Models.Outputs.GetBrandsDTO>();
			Models.Outputs.GetBrandsDTO resp = new Models.Outputs.GetBrandsDTO();
			dto.objname = "GetBrands";
			try
			{
				resp = getbrands();

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetModuleDTO> GetModules(Input<Models.Inputs.GetModule> obj)
		{
			DTO<Models.Outputs.GetModuleDTO> dto = new DTO<Models.Outputs.GetModuleDTO>();
			Models.Outputs.GetModuleDTO resp = new Models.Outputs.GetModuleDTO();
			dto.objname = "GetModules";
			try
			{
				if (string.IsNullOrEmpty(obj.input.brandcd))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = getmodule(Convert.ToInt32(obj.input.brandcd));

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetItemDescriptionDTO> GetItemDescription(Input<Models.Inputs.GetItemDescription> obj)
		{
			DTO<Models.Outputs.GetItemDescriptionDTO> dto = new DTO<Models.Outputs.GetItemDescriptionDTO>();
			Models.Outputs.GetItemDescriptionDTO resp = new Models.Outputs.GetItemDescriptionDTO();
			dto.objname = "GetItemDescription";
			try
			{
                if (!string.IsNullOrEmpty(obj.input.itemcode))
                {
                    resp = getitems(obj.input.itemcode);
                    dto.response = resp;
                    dto.status = new Models.Status(0);
                }
                else
                {
                    dto.status = new Models.Status(1);
                }
            }
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetBySerialNumberDTO> GetBySerialNumber(Input<Models.Inputs.GetBySerialNumber> obj)
		{
			DTO<Models.Outputs.GetBySerialNumberDTO> dto = new DTO<Models.Outputs.GetBySerialNumberDTO>();
			Models.Outputs.GetBySerialNumberDTO resp = new Models.Outputs.GetBySerialNumberDTO();
			dto.objname = "GetBySerialNumber";
			try
			{
				if (string.IsNullOrEmpty(obj.input.serialnumber))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = getbyserialno(obj.input.serialnumber);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetByBrandandModuleDTO> GetByBrandandModule(Input<Models.Inputs.GetByBrandandModule> obj)
		{
			DTO<Models.Outputs.GetByBrandandModuleDTO> dto = new DTO<Models.Outputs.GetByBrandandModuleDTO>();
			Models.Outputs.GetByBrandandModuleDTO resp = new Models.Outputs.GetByBrandandModuleDTO();
			dto.objname = "GetByBrandandModule";
			try
			{
				if (string.IsNullOrEmpty(obj.input.brand) || string.IsNullOrEmpty(obj.input.module))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = getbybrandnmodule(obj.input.brand, obj.input.module);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.AddEquipmentDTO> AddEquipment(Input<Models.Inputs.AddEquipment> obj)
		{
			DTO<Models.Outputs.AddEquipmentDTO> dto = new DTO<Models.Outputs.AddEquipmentDTO>();
			Models.Outputs.AddEquipmentDTO resp = new Models.Outputs.AddEquipmentDTO();
			dto.objname = "AddEquipment";
			try
			{
				if (string.IsNullOrEmpty(obj.input.wono) || string.IsNullOrEmpty(obj.input.faultcode) || string.IsNullOrEmpty(obj.input.actioncode))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = addequip(obj.input);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.AddUsedItemsDTO> AddUsedItems(Input<Models.Inputs.AddUsedItems> obj)
		{
			DTO<Models.Outputs.AddUsedItemsDTO> dto = new DTO<Models.Outputs.AddUsedItemsDTO>();
			Models.Outputs.AddUsedItemsDTO resp = new Models.Outputs.AddUsedItemsDTO();
			dto.objname = "AddUsedItems";
			try
			{
				if (string.IsNullOrEmpty(obj.input.wono) || string.IsNullOrEmpty(obj.input.unitserialno)
					|| string.IsNullOrEmpty(obj.input.Itemcode) || string.IsNullOrEmpty(obj.input.module))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = adduseditem(obj.input);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.UpdateWorkOrderDetailsDTO> UpdateWorkOrderDetails(Input<Models.Inputs.UpdateWorkOrderDetails> obj)
		{
			DTO<Models.Outputs.UpdateWorkOrderDetailsDTO> dto = new DTO<Models.Outputs.UpdateWorkOrderDetailsDTO>();
			Models.Outputs.UpdateWorkOrderDetailsDTO resp = new Models.Outputs.UpdateWorkOrderDetailsDTO();
			dto.objname = "UpdateWorkOrderDetails";
			try
			{
				if (string.IsNullOrEmpty(obj.input.woid) || string.IsNullOrEmpty(obj.input.wostatusid) || string.IsNullOrEmpty(obj.input.wostatusactionid))
				{
					dto.status = new Models.Status(800);
					return dto;
				}
				resp = updatewo(Convert.ToInt32(obj.input.woid), Convert.ToInt32(obj.input.wostatusid), Convert.ToInt32(obj.input.wostatusactionid), obj.input.techremark, obj.input.customerremark);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}
		public DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersbySearch(Input<Models.Inputs.GetClosedWorkOrderBySearch> obj)
		{
			DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> dto = new DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO>();
			Models.Outputs.GetClosedWorkOrdersbyGroupDTO resp = new Models.Outputs.GetClosedWorkOrdersbyGroupDTO();
			dto.objname = "GetClosedWorkOrdersbySearch";
			try
			{
				resp = getclosedworkorderbysearch(obj.input);

				dto.response = resp;
				dto.status = new Models.Status(0);
			}
			catch (Exception ex)
			{
				dto.status = new Models.Status(1);
			}
			return dto;
		}

		#endregion

		#region Private Methods

		private bool isvalidlogin(string username, string password)
		{
			var _group = db.TB_GROUPS.Where(x => x.TB_USERNAME == username && x.TB_PASSWORD == password && x.TB_ACTIVE == true).FirstOrDefault();
			if (_group == null)
			{
				return false;
			}
			group = _group;

			return true;
		}
		private Models.Outputs.WorkOrderByGroupDTO getworkordersbygroup(string groupcd)
		{
			Models.Outputs.WorkOrderByGroupDTO resp = new Models.Outputs.WorkOrderByGroupDTO();
			List<WorkOrder> wolist = new List<WorkOrder>();

			//var rows = db.sp_Get_Group_Orders(groupcd).ToList();


			var rows = db.TB_WO.Where(x => x.GRP_CD == groupcd && x.WO_STATUS == 2).OrderByDescending(x => x.ENTRY_DATE).ToList();

			foreach (var row in rows)
			{
				WorkOrder wo = new WorkOrder();
				wo.complno = row.COMPL_NO;
				wo.custcd = row.CUS_CD;
				wo.groupcd = row.GRP_CD;
				wo.manualwono = row.MANUAL_WO_NO;
				wo.type = row.TYPE;
				wo.servicetype = row.CONT_TYPE;
				wo.contractno = row.CNT_CD;
				wo.woid = row.TB_ID.ToString();
				wo.wono = row.WO_NO;
				wo.techremark = row.TB_TECH_REMARK;
				wo.customerremark = row.TB_CUST_REMARK;
				//var customer = getcustomerbycustcd(row.CUS_CD);
				wo.customerInfo = new CustomerInfo()
				{
					custcd = row.CUS_CD,
					customername = row.CUS_NAM_ENG,
					mobile = row.MOBILE,
					telephone = row.TEL
				};

				//var contract = db.TB_CONTRACTS.Where(x => x.CNT_CD == row.CNT_CD).FirstOrDefault();
				//if (contract != null)
				//{
				wo.contractInfo = new ContractInfo()
				{
					ExpirationDate = row.EXP_DT,
					ProjectNo = row.CNT_CD
				};
				//}

				wo.orderAddress = new WorkOrderAddress()
				{
					//area = row.NAM_ENG,
					avenue = row.AVENUE,
					block = row.BLOCK,
					houseno = row.HOUSE_NO,
					plot_no = row.PLOT_NO,
					street = row.STREET,
				};
				if (!string.IsNullOrEmpty(row.AREA_CD))
				{
					wo.orderAddress.area = getareabyareid(Convert.ToInt32(row.AREA_CD));
				}

				wo.wostatusaction = getwostatusaction(Convert.ToInt32(row.WO_STATUS));
				//wo.wostatusaction = new wostatusaction()
				//{
				//	wostatusactionid = row.actionID.ToString(),
				//	statusactionname = row.TB_STATUS_ACTION_NAME
				//};
				wolist.Add(wo);
			}
			resp.workorders = wolist;

			return resp;
		}
		private Models.Outputs.WorkOrderDetailsDTO getworkordersbyid(int woid)
		{
			try
			{
				Models.Outputs.WorkOrderDetailsDTO resp = new Models.Outputs.WorkOrderDetailsDTO();

				var row = db.TB_WO.Where(x => x.TB_ID == woid).FirstOrDefault();
				if (row != null)
				{
					resp.complno = row.COMPL_NO;
					resp.custcd = row.CUS_CD;
					resp.groupcd = row.GRP_CD;
					resp.manualwono = row.MANUAL_WO_NO;
					resp.woid = row.TB_ID.ToString();
					resp.wono = row.WO_NO;
					resp.type = row.TYPE;
					resp.contractno = row.CNT_CD;
					resp.servicetype = row.CONT_TYPE;
					//resp.wostatus = getwostatus(Convert.ToInt32(row.WO_STATUS));
					resp.wostatusaction = getwostatusaction(Convert.ToInt32(row.WO_STATUS));
					resp.techtimein = row.TECH_TIME_IN;
					resp.techtimeout = row.TECH_TIME_OUT;
					resp.remarks = row.REMARKS;
					//resp.addresscivilid = row.ADDRESS_CIVILID;
					resp.area = getareabyareid(Convert.ToInt32(row.AREA_CD));
					resp.avenue = row.AVENUE;
					resp.block = row.BLOCK;
					resp.civilid = row.CIVILID;
					resp.custcd = row.CUS_CD;
					resp.customername = row.CUS_NAM_ENG;
					resp.houseno = row.HOUSE_NO;
					resp.mobile = row.MOBILE;
					resp.plot_no = row.PLOT_NO;
					resp.street = row.STREET;
					resp.telephone = row.TEL;
					//resp.customer = getcustomerbycustcd(row.CUS_CD);
					resp.equipment = getequipment(row.WO_NO);
					resp.items = getitemsbywono(row.WO_NO);
				}

				return resp;

			}
			catch (Exception ex)
			{
				return null;
			}
		}

		private WorkOrderAddress getOrderAddress(int woid)
		{
			WorkOrderAddress resp = new WorkOrderAddress();

			var row = db.TB_WO.Where(x => x.TB_ID == woid).OrderByDescending(x => x.ENTRY_DATE).FirstOrDefault();
			if (row != null)
			{
				resp.area = getareabyareid(Convert.ToInt32(row.AREA_CD));
				resp.avenue = row.AVENUE;
				resp.block = row.BLOCK;
				resp.houseno = row.HOUSE_NO;
				resp.plot_no = row.PLOT_NO;
				resp.street = row.STREET;
			}

			return resp;
		}
		private string getareabyareid(int id)
		{
			var row = db.TB_AREA.Where(x => x.AREA_CD == id).FirstOrDefault();
			if (row != null)
			{
				return row.NAM_ENG;
			}
			return "";
		}
		private customer getcustomerbycustcd(string custcd)
		{
			customer cust = new customer();

			var row = db.sp_Get_Customer_Details(custcd, "", "", "").FirstOrDefault();

			if (row != null)
			{
				cust.custcd = row.TB_CUSTOMER_REF_ID;
				cust.customerid = row.TB_ID.ToString();
				cust.customername = row.TB_FIRST_NAME;
				cust.mobile = row.TB_MOBILE1;
				cust.telephone = row.TB_PHONE1;
				cust.customeraddresses = getcustomeraddressesbycustcd(custcd);
			}

			return cust;
		}
		private List<customeraddress> getcustomeraddressesbycustcd(string custcd)
		{
			List<customeraddress> custaddlst = new List<customeraddress>();

			var rows = db.sp_Get_CustomerAddress(0, custcd, "").ToList();

			foreach (var row in rows)
			{
				customeraddress custadd = new customeraddress();
				custadd.addressname = row.TB_ADDRESS_NAME;
				custadd.area = row.AREA_NAME;
				custadd.avenue = row.AVENUE;
				custadd.block = row.BLOCK;
				custadd.custcd = row.CUS_CD;
				custadd.customeraddressid = row.TB_ID.ToString();
				custadd.geolati = row.TB_GEO_LATI;
				custadd.geolong = row.TB_GEO_LONG;
				custadd.houseno = row.HOUSE_NO;
				custadd.plot_no = row.PLOT_NO;
				custadd.street = row.STREET;

				custaddlst.Add(custadd);
			}

			return custaddlst;
		}
		private Models.Outputs.GetLOVDTO getalllovs()
		{
			Models.Outputs.GetLOVDTO resp = new GetLOVDTO();

			List<LOV> lovlist = new List<LOV>();

			var rows = db.vw_LOV.Where(x => x.TB_DEL == false).ToList();
			foreach (var row in rows)
			{
				LOV lv = new LOV();
				lv.lovid = row.TB_ID.ToString();
				lv.name = row.TB_NAME;
				lv.namear = row.TB_NAMEAR;
				lv.type = row.TB_TYPE;
				lovlist.Add(lv);
			}
			resp.lovs = lovlist;
			return resp;

		}
		private Models.Outputs.GetLOVDTO getlovsbytype(string type)
		{
			Models.Outputs.GetLOVDTO resp = new GetLOVDTO();

			List<LOV> lovlist = new List<LOV>();

			var rows = db.vw_LOV.Where(x => x.TB_TYPE == type && x.TB_DEL == false).ToList();
			foreach (var row in rows)
			{
				LOV lv = new LOV();
				lv.lovid = row.TB_ID.ToString();
				lv.name = row.TB_NAME;
				lv.namear = row.TB_NAMEAR;
				lv.type = row.TB_TYPE;
				lovlist.Add(lv);
			}
			resp.lovs = lovlist;
			return resp;

		}
		private Models.Outputs.wostatusactionDTO getstatusaction(string wostatusid)
		{
			Models.Outputs.wostatusactionDTO resp = new Models.Outputs.wostatusactionDTO();
			List<wostatusaction> wosalst = new List<wostatusaction>();

			var rows = db.vw_WO_STATUS_ACTION.Where(x => x.TB_WO_STATUS_LOVID == wostatusid).ToList();
			foreach (var row in rows)
			{
				wostatusaction wosa = new wostatusaction();
				wosa.statusactionname = row.TB_STATUS_ACTION_NAME;
				wosa.wostatusactionid = row.TB_ID.ToString();
				//wosa.wostatuslovid = row.TB_WO_STATUS_LOVID;
				wosalst.Add(wosa);
			}
			resp.wostatusactions = wosalst;
			return resp;
		}
		private Models.Outputs.FaultDTO getfaults()
		{
			Models.Outputs.FaultDTO resp = new Models.Outputs.FaultDTO();
			List<Fault> flist = new List<Fault>();

			var rows = db.vw_FAULT.OrderByDescending(x => x.TB_ID).ToList();

			foreach (var row in rows)
			{
				Fault f = new Fault();
				f.faultid = row.TB_ID.ToString();
				f.faultcode = row.FAULT_CODE;
				f.faultdesc = row.FAULT_DESC;
				flist.Add(f);
			}
			resp.faults = flist;
			return resp;
		}
		private Models.Outputs.GetAactionFaultDTO getactionfault(string faultcode)
		{
			Models.Outputs.GetAactionFaultDTO resp = new Models.Outputs.GetAactionFaultDTO();
			List<ActionFault> aflst = new List<ActionFault>();

			var rows = db.vw_ACTION_FAULT.Where(x => x.FAULT_CODE == faultcode).OrderByDescending(x => x.TB_ID).ToList();

			foreach (var row in rows)
			{
				ActionFault af = new ActionFault();
				af.actioncode = row.ACTION_CODE;
				af.actionid = row.TB_ID.ToString();
				af.actiondesc = row.ACTION_DESC;
				af.faultcode = row.FAULT_CODE;
				aflst.Add(af);
			}
			resp.actionfaults = aflst;
			return resp;
		}
		private Models.Outputs.GetGroupsDTO getgroups()
		{
			Models.Outputs.GetGroupsDTO resp = new GetGroupsDTO();
			var rows = db.sp_Get_Groups(0, 1);
			List<Group> grplst = new List<Group>();

			foreach (var row in rows)
			{
				Group grp = new Group();
				grp.groupid = row.TB_ID.ToString();
				grp.groupcd = row.GROUP_CD;
				grp.groupdesc = row.GROUP_DSC;
				grp.vanno = row.VAN_NO;
				grp.vandesc = row.VAN_DSC;
				grp.employeeno = row.EMPLOYEE_NUMBER;
				grp.employeename = row.EMPLOYEE_NAME;
				grp.remarks = row.REMARKS;
				grplst.Add(grp);
			}
			resp.groups = grplst;

			return resp;
		}
		private Models.Outputs.UpdateWorkOrderStatusDTO updateworkorder(int woid, int wostatusid, string groupcd)
		{
			Models.Outputs.UpdateWorkOrderStatusDTO resp = new UpdateWorkOrderStatusDTO();

			var row = db.TB_WO.Where(x => x.TB_ID == woid).FirstOrDefault();

			if (row != null)
			{
				row.WO_STATUS = wostatusid;
				row.UPDATE_DATE = DateTime.Now;
				row.UPDATE_USER_NAME = groupcd;
				db.SaveChanges();

				if (row.WO_STATUS >= 6)
				{
					var cmplrw = db.TB_CALL_CENTER.Where(x => x.COMPL_NO == row.COMPL_NO).FirstOrDefault();
					if (cmplrw != null)
					{
						cmplrw.WO_STATUS = wostatusid;
						cmplrw.MTN_STATUS = 63;
						db.SaveChanges();
					}
				}
			}

			return resp;
		}
		private Models.Outputs.GetClosedWorkOrdersbyGroupDTO getclosedworkordergroup(string groupcd)
		{
			Models.Outputs.GetClosedWorkOrdersbyGroupDTO resp = new GetClosedWorkOrdersbyGroupDTO();
			List<WorkOrder> lwrk = new List<WorkOrder>();

			var rows = db.TB_WO.Where(x => x.GRP_CD == groupcd && x.WO_STATUS >= 6).OrderByDescending(x => x.TB_ID).Take(100).ToList();

			foreach (var row in rows)
			{
				WorkOrder wrk = new WorkOrder();
				wrk.complno = row.COMPL_NO;
				wrk.custcd = row.CUS_CD;
				wrk.groupcd = row.GRP_CD;
				wrk.manualwono = row.MANUAL_WO_NO;
				wrk.woid = row.TB_ID.ToString();
				wrk.wono = row.WO_NO;
				wrk.type = row.TYPE;
				wrk.servicetype = row.CONT_TYPE;
				wrk.contractno = row.CNT_CD;
				//wrk.wopriority = getpriority(Convert.ToInt32(row.TB_PRIORITY_LOVID));
				//wrk.wostatus = getwostatus(Convert.ToInt32(row.WO_STATUS));
				wrk.wostatusaction = getwostatusaction(Convert.ToInt32(row.WO_STATUS));
				wrk.customerremark = row.TB_CUST_REMARK;
				wrk.techremark = row.TB_TECH_REMARK;
				lwrk.Add(wrk);
			}
			resp.workorders = lwrk;


			return resp;
		}
		private Models.Outputs.GetClosedWorkOrdersbyGroupDTO getclosedworkorderbysearch(Models.Inputs.GetClosedWorkOrderBySearch obj)
		{
			Models.Outputs.GetClosedWorkOrdersbyGroupDTO resp = new GetClosedWorkOrdersbyGroupDTO();
			List<WorkOrder> lwrk = new List<WorkOrder>();

			var rows = db.TB_WO.Where(x => x.GRP_CD == obj.groupcd && x.WO_STATUS >= 6 && (x.WO_NO == obj.wono || x.COMPL_NO == obj.complno || x.CUS_CD == obj.custcd))
						 .OrderByDescending(x => x.TB_ID).ToList();

			foreach (var row in rows)
			{
				WorkOrder wrk = new WorkOrder();
				wrk.complno = row.COMPL_NO;
				wrk.custcd = row.CUS_CD;
				wrk.groupcd = row.GRP_CD;
				wrk.manualwono = row.MANUAL_WO_NO;
				wrk.woid = row.TB_ID.ToString();
				wrk.wono = row.WO_NO;
				wrk.type = row.TYPE;
				wrk.servicetype = row.CONT_TYPE;
				wrk.contractno = row.CNT_CD;
				//wrk.wopriority = getpriority(Convert.ToInt32(row.TB_PRIORITY_LOVID));
				wrk.wostatusaction = getwostatusaction(Convert.ToInt32(row.WO_STATUS));
				//wrk.wostatusaction = getwostatusaction(Convert.ToInt32(row.TB_STATUS_ACTION_ID));
				wrk.customerremark = row.TB_CUST_REMARK;
				wrk.techremark = row.TB_TECH_REMARK;
				lwrk.Add(wrk);
			}
			resp.workorders = lwrk;


			return resp;
		}
		private Models.Outputs.Priority getpriority(int pid)
		{
			Priority p = new Models.Outputs.Priority();

			var row = db.TB_LOV.Where(x => x.TB_ID == pid && x.TB_DEL == false).FirstOrDefault();

			if (row != null)
			{
				p.prioritylovid = row.TB_ID.ToString();
				p.priorityname = row.TB_NAME;
			}
			return p;
		}
		private Models.Outputs.WOStatus getwostatus(int wosid)
		{
			WOStatus wos = new Models.Outputs.WOStatus();

			var row = db.TB_LOV.Where(x => x.TB_ID == wosid).FirstOrDefault();
			if (row != null)
			{
				wos.wostatuslovid = row.TB_ID.ToString();
				wos.wostatusname = row.TB_NAME;
			}
			return wos;
		}
		private Models.Outputs.wostatusaction getwostatusaction(int wosaid)
		{
			wostatusaction wosa = new Models.Outputs.wostatusaction();

			var row = db.TB_WO_STATUS_ACTION.Where(x => x.TB_ID == wosaid).FirstOrDefault();
			if (row != null)
			{
				wosa.wostatusactionid = row.TB_ID.ToString();
				//wosa.wostatuslovid = row.TB_WO_STATUS_LOVID.ToString();
				wosa.statusactionname = row.TB_STATUS_ACTION_NAME;
			}
			return wosa;
		}
		private Models.Outputs.GetBrandsDTO getbrands()
		{
			Models.Outputs.GetBrandsDTO resp = new GetBrandsDTO();

			List<Brands> lbrand = new List<Brands>();

			var rows = db.TB_BRAND.ToList();

			foreach (var row in rows)
			{
				Brands brand = new Brands();

				brand.brandcd = row.BRAND_CD.ToString();
				brand.brandname = row.NAM_ENG;
				lbrand.Add(brand);
			}

			resp.brands = lbrand;

			return resp;
		}
		private Models.Outputs.GetModuleDTO getmodule(int brandcd)
		{
			Models.Outputs.GetModuleDTO resp = new GetModuleDTO();

			List<Module> lmod = new List<Module>();

			var rows = db.TB_MODULE.Where(x => x.BRAND_CD == brandcd).ToList();

			foreach (var row in rows)
			{
				Module mod = new Module();
				mod.brandcd = row.BRAND_CD.ToString();
				mod.capacity = row.CAPACITY;
				mod.modulecd = row.MODEL_CD;
				mod.modulename = row.NAM_ENG;
				lmod.Add(mod);
			}
			resp.modules = lmod;

			return resp;
		}
		private Models.Outputs.GetItemDescriptionDTO getitems(string itemCode)
		{
			Models.Outputs.GetItemDescriptionDTO resp = new GetItemDescriptionDTO();

			List<Items> litem = new List<Items>();

			var rows = db.TB_ITEM_DETAIL.Where(i=>i.ITEM_CODE.StartsWith(itemCode)).Take(15).ToList();

			foreach (var row in rows)
			{
				Items item = new Items();
				item.itemcode = row.ITEM_CODE;
				item.itemname = row.DESCRIPTION;
				item.uom = row.UOM;

				litem.Add(item);
			}
			resp.items = litem;

			return resp;
		}
		private Models.Outputs.GetBySerialNumberDTO getbyserialno(string serialno)
		{
			Models.Outputs.GetBySerialNumberDTO resp = new GetBySerialNumberDTO();

			var row = db.TB_ITEMS_SERIAL.Where(x => x.SERIAL_NUMBER.Contains(serialno)).FirstOrDefault();

			if (row != null)
			{
				resp.brand = row.BRAND;
				resp.itemcode = row.ITEM_CODE;
				resp.module = row.DESCRIPTION;
				resp.serialnumber = row.SERIAL_NUMBER;
			}
			return resp;
		}
		private Models.Outputs.GetByBrandandModuleDTO getbybrandnmodule(string brand, string module)
		{
			Models.Outputs.GetByBrandandModuleDTO resp = new GetByBrandandModuleDTO();
			List<GetSerialsByBrandandModule> serlst = new List<GetSerialsByBrandandModule>();

			var rows = db.TB_ITEMS_SERIAL.Where(x => x.BRAND == brand && x.DESCRIPTION == module).ToList();

			foreach (var row in rows)
			{
				GetSerialsByBrandandModule ser = new GetSerialsByBrandandModule();
				ser.brand = row.BRAND;
				ser.itemcode = row.ITEM_CODE;
				ser.module = row.DESCRIPTION;
				ser.serialnumber = row.SERIAL_NUMBER;
				serlst.Add(ser);
			}
			resp.serials = serlst;
			return resp;

		}
		private Models.Outputs.AddEquipmentDTO addequip(Models.Inputs.AddEquipment obj)
		{
			Models.Outputs.AddEquipmentDTO resp = new AddEquipmentDTO();

			TB_WO_DTL wdtl = new TB_WO_DTL();

			wdtl.WO_NO = obj.wono;
			wdtl.ACTION_DONE = obj.actioncode;
			wdtl.AMB_TEMP = obj.ambtemperature;
			wdtl.AMP = obj.amp;
			wdtl.BRAND_CD = obj.brandcd;
			wdtl.D_PR = obj.dpr;
			wdtl.FAULT_CODE = obj.faultcode;
			wdtl.GRP_CD = obj.group;
			wdtl.ITM_CD = obj.modulecd;
			wdtl.R_DUCT_TEMP = obj.rducttemp;
			wdtl.ROOM_TEMP = obj.roomtemperature;
			wdtl.S_DUCT_TEMP = obj.sducttemp;
			wdtl.SNO = obj.serialnumber;
			wdtl.S_PR = obj.spr;
			wdtl.VOLT = obj.volt;
			db.TB_WO_DTL.Add(wdtl);
			db.SaveChanges();


			resp.action = getactionfaultbycode(wdtl.ACTION_DONE);
			resp.ambtemperature = wdtl.AMB_TEMP;
			resp.amp = wdtl.AMP;
			resp.brandcd = wdtl.BRAND_CD;
			resp.dpr = wdtl.D_PR;
			resp.fault = getfaultbycode(wdtl.FAULT_CODE);
			resp.group = wdtl.GRP_CD;
			resp.modulecd = wdtl.ITM_CD;
			resp.rducttemp = wdtl.R_DUCT_TEMP;
			resp.roomtemperature = wdtl.ROOM_TEMP;
			resp.sducttemp = wdtl.S_DUCT_TEMP;
			resp.serialnumber = wdtl.SNO;
			resp.spr = wdtl.S_PR;
			resp.volt = wdtl.VOLT;
			resp.wono = wdtl.WO_NO;

			return resp;
		}
		private Models.Outputs.AddUsedItemsDTO adduseditem(Models.Inputs.AddUsedItems obj)
		{
			Models.Outputs.AddUsedItemsDTO resp = new AddUsedItemsDTO();

			TB_ASSIGN_SPAREPARTS asp = new TB_ASSIGN_SPAREPARTS();
			asp.ENTRY_DATE = DateTime.Now;
			asp.SERIALNO = obj.unitserialno;
			asp.ITEM_CODE = obj.Itemcode;
			asp.ITEM_DESCRIPTION = obj.module;
			asp.QUANTITY = obj.quantity;
			asp.TB_WONO = obj.wono;
			asp.UOM = obj.uom;
			db.TB_ASSIGN_SPAREPARTS.Add(asp);
			db.SaveChanges();

			resp.uom = asp.UOM;
			resp.unitserialno = asp.SERIALNO;
			resp.Itemcode = asp.ITEM_CODE;
			resp.module = asp.ITEM_DESCRIPTION;
			resp.quantity = asp.QUANTITY;
			//resp.price = asp.TB_UNITPRICE;
			resp.wono = asp.TB_WONO;

			return resp;
		}
		private Models.Outputs.UpdateWorkOrderDetailsDTO updatewo(int woid, int wostatusid, int wostatusactionid, string techrmk, string custrmk)
		{
			Models.Outputs.UpdateWorkOrderDetailsDTO resp = new UpdateWorkOrderDetailsDTO();

			var row = db.TB_WO.Where(x => x.TB_ID == woid).FirstOrDefault();

			if (row != null)
			{
				row.WO_STATUS = wostatusactionid;
				row.TB_TECH_REMARK = techrmk;
				row.TB_CUST_REMARK = custrmk;
				db.SaveChanges();


				var cmplrw = db.TB_CALL_CENTER.Where(x => x.COMPL_NO == row.COMPL_NO).FirstOrDefault();
				if (cmplrw != null)
				{
					cmplrw.WO_STATUS = wostatusactionid;
					cmplrw.MTN_STATUS = wostatusid;
					db.SaveChanges();
				}

			}
			return resp;
		}
		private List<AddEquipmentDTO> getequipment(string wono)
		{
			List<AddEquipmentDTO> resp = new List<AddEquipmentDTO>();

			var rows = db.TB_WO_DTL.Where(x => x.WO_NO == wono).ToList();

			foreach (var row in rows)
			{
				AddEquipmentDTO ae = new AddEquipmentDTO();
				ae.action = getactionfaultbycode(row.ACTION_DONE);
				ae.ambtemperature = row.AMB_TEMP;
				ae.amp = row.AMP;
				ae.brandcd = row.BRAND_CD;
				ae.dpr = row.D_PR;
				ae.fault = getfaultbycode(row.FAULT_CODE);
				ae.group = row.GRP_CD;
				ae.modulecd = row.ITM_CD;
				ae.rducttemp = row.R_DUCT_TEMP;
				ae.roomtemperature = row.ROOM_TEMP;
				ae.sducttemp = row.S_DUCT_TEMP;
				ae.serialnumber = row.SNO;
				ae.spr = row.S_PR;
				ae.volt = row.VOLT;
				ae.wono = row.WO_NO;

				resp.Add(ae);
			}

			return resp;
		}
		private List<AddUsedItemsDTO> getitemsbywono(string wono)
		{
			List<AddUsedItemsDTO> resp = new List<AddUsedItemsDTO>();

			var rows = db.TB_ASSIGN_SPAREPARTS.Where(x => x.TB_WONO == wono).ToList();

			foreach (var row in rows)
			{
				AddUsedItemsDTO ui = new AddUsedItemsDTO();
				ui.Itemcode = row.ITEM_CODE;
				ui.module = row.ITEM_DESCRIPTION;
				ui.quantity = row.QUANTITY;
				ui.wono = row.TB_WONO;
				ui.uom = row.UOM;
				ui.unitserialno = row.SERIALNO;
				resp.Add(ui);
			}

			return resp;
		}
		private Fault getfaultbycode(string faultcode)
		{
			Models.Outputs.Fault resp = new Models.Outputs.Fault();

			var row = db.vw_FAULT.Where(x => x.FAULT_CODE == faultcode).FirstOrDefault();
			if (row != null)
			{
				resp.faultid = row.TB_ID.ToString();
				resp.faultcode = row.FAULT_CODE;
				resp.faultdesc = row.FAULT_DESC;
			}
			return resp;
		}
		private ActionFault getactionfaultbycode(string actioncode)
		{
			Models.Outputs.ActionFault resp = new Models.Outputs.ActionFault();

			var row = db.vw_ACTION_FAULT.Where(x => x.ACTION_CODE == actioncode).FirstOrDefault();

			if (row != null)
			{
				resp.actioncode = row.ACTION_CODE;
				resp.actionid = row.TB_ID.ToString();
				resp.actiondesc = row.ACTION_DESC;
				resp.faultcode = row.FAULT_CODE;
			}
			return resp;
		}

		#endregion

	}
}
