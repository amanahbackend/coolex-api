﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMSCoolexAPI.Models;

namespace MMSCoolexAPI.BL.Interface
{
    public interface IMMS
    {
        DTO<Models.Outputs.LoginDTO> Login(Input<Models.Inputs.Login> obj);
        DTO<Models.Outputs.WorkOrderByGroupDTO> GetWorkOrdersByGroup(Input<Models.Inputs.WorkOrderByGroup> obj);
        DTO<Models.Outputs.WorkOrderDetailsDTO> GetWorkOrderDetails(Input<Models.Inputs.WorkOrderDetails> obj);
        DTO<Models.Outputs.GetLOVDTO> GetALLLOVs(Input<Models.Inputs.GetLOV> obj);
        DTO<Models.Outputs.GetLOVDTO> GetLOVByType(Input<Models.Inputs.GetLOVbyType> obj);
        DTO<Models.Outputs.wostatusactionDTO> GetWorkOrderStatusAction(Input<Models.Inputs.wostatusaction> obj);
        DTO<Models.Outputs.FaultDTO> GetFaults(Input<Models.Inputs.GetFault> obj);
        DTO<Models.Outputs.GetAactionFaultDTO> GetActionFaults(Input<Models.Inputs.GetActionFault> obj);
        DTO<Models.Outputs.GetGroupsDTO> GetGroups(Input<Models.Inputs.GetGroups> obj);
        //DTO<Models.Outputs.UpdateWorkOrderStatusDTO> UpdateWorkOrderStatus(Input<Models.Inputs.UpdateWorkOrderStatus> obj);
        DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersByGroup(Input<Models.Inputs.GetClosedWorkOrdersByGroup> obj);
        DTO<Models.Outputs.GetBrandsDTO> GetBrands(Input<Models.Inputs.GetBrands> obj);
        DTO<Models.Outputs.GetModuleDTO> GetModules(Input<Models.Inputs.GetModule> obj);
        DTO<Models.Outputs.GetItemDescriptionDTO> GetItemDescription(Input<Models.Inputs.GetItemDescription> obj);
        DTO<Models.Outputs.GetBySerialNumberDTO> GetBySerialNumber(Input<Models.Inputs.GetBySerialNumber> obj);
        DTO<Models.Outputs.GetByBrandandModuleDTO> GetByBrandandModule(Input<Models.Inputs.GetByBrandandModule> obj);
        DTO<Models.Outputs.AddEquipmentDTO> AddEquipment(Input<Models.Inputs.AddEquipment> obj);
        DTO<Models.Outputs.AddUsedItemsDTO> AddUsedItems(Input<Models.Inputs.AddUsedItems> obj);
        DTO<Models.Outputs.UpdateWorkOrderDetailsDTO> UpdateWorkOrderDetails(Input<Models.Inputs.UpdateWorkOrderDetails> obj);
        DTO<Models.Outputs.GetClosedWorkOrdersbyGroupDTO> GetClosedWorkOrdersbySearch(Input<Models.Inputs.GetClosedWorkOrderBySearch> obj);
    }
}
